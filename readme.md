# ***AllAllow*** #
## **Allow online and offline players with the same BungeeCord instance.** ##
# ----------------------------------------------- #

It allows online players to be authenticated through Mojang and skip /login commands. Where as offline players are authenticated through LogIt <Currently LogIt is the only supported one but more very soon!>.

## **DISCLAIMER** ##
###**AllAllow is in beta stages. Please be wary that there may be bugs and incomplete features. Any found should be reported as documented below. EXPECT BUGS. Because of this, I take no responsibility of your server being "hacked", crashes or data loss. Further upstream in the project, this will not be so. Use this commercially at your own risk!** ###

**Installation**

* Install LogIt to one of your bukkit servers.
* Install AllAllow to the same server.
* Install AllAllow to the BungeeCord server.
* Ensure online-mode: true in BungeeCord's config.yml.
* Restart all the effected servers.
* Log into the server and go to the bukkit server with LogIt installed.
* Type command **/aa setserver**


**Information**

* AllAllow uses plugin messages to communicate.
* There is an API for the plugin messages making it usable by other popular authentication plugins.
* 10% of the money goes to SpigotMC.
* The other 90% is likley to go on fuel for Cory Redmond...
* Fuel for Cory Redmond is Rockstar.
* This plugin has been worked on for a year an a half.
* Cory Redmond looses his temper and starts again a lot.


**Todo**

* Add secret feature [ip].
* Add secret feature [.net].
* Please suggest more (Not "Add support for {Bukkit Auth Plugin}")
* Work on the Bukkit Auth class.
* Find more information to put on this.
* Secure Socket for stuff.


**Permissions**

* allallow.admin - All commands.
* allallow.bypass [TOTO] -  Allows the user to be auto logged in. Even in offline mode!
* allallow.force [TOTO] - Forces the user to login.

[**Short Demo Video**](https://www.youtube.com/watch?v=rdmZyhgeOik)

Sorry for the swearing. And yawning if you're tired.
In fact im'ma put a disclaimer on here.** BAD VIDEO **

**How It Works**

* We check if the username is premium through [minecraft.net]('https://minecraft.net/haspaid.jsp?user=[INSERT_USERNAME_HERE). Once we've established how to authenticate them, we do so. Soon we're going to get methods of allowing players to bypass and change certain authentication methods, for the greater good!

* [Simulation](http://scratch.mit.edu/projects/embed/20645061/?autostart=false)
* [Video](https://www.youtube.com/watch?v=REqdz1aXXpA)